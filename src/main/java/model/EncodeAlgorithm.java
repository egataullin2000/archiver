package model;

public enum EncodeAlgorithm {
    RUN_LENGTH_ENCODING, BURROWS_WHEELER_TRANSFORM, HUFFMAN_CODING
}
