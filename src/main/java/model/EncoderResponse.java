package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EncoderResponse {
    private List<EncodeAlgorithm> usedAlgorithms = new ArrayList<>();
    private byte[] result;

    public void addUsedAlgorithms(EncodeAlgorithm... algorithms) {
        Collections.addAll(usedAlgorithms, algorithms);
    }

    public List<EncodeAlgorithm> getUsedAlgorithms() {
        return usedAlgorithms;
    }

    public void setResult(byte[] result) {
        this.result = result;
    }

    public byte[] getResult() {
        return result;
    }

    public int getResultSize() {
        return result.length;
    }
}
