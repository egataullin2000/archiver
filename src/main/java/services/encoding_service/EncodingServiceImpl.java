package services.encoding_service;

import model.EncoderResponse;
import model.Pair;

import java.util.*;
import java.util.stream.Collectors;

import static model.EncodeAlgorithm.*;

public class EncodingServiceImpl implements EncodingService {
    @Override
    public EncoderResponse encode(byte[] source) {
        EncoderResponse response = new EncoderResponse();
        response.setResult(source);

        byte[] bytes = doRunLengthEncoding(source);
        if (bytes.length < response.getResultSize()) {
            response.addUsedAlgorithms(RUN_LENGTH_ENCODING);
            response.setResult(bytes);
        }
        bytes = doRunLengthEncoding(doBwt(response.getResult()));
        if (bytes.length < response.getResultSize()) {
            response.addUsedAlgorithms(BURROWS_WHEELER_TRANSFORM, RUN_LENGTH_ENCODING);
            response.setResult(bytes);
        }
        bytes = doHuffmanCoding(bytes);
        if (bytes.length < response.getResultSize()) {
            response.addUsedAlgorithms(HUFFMAN_CODING);
            response.setResult(bytes);
        }

        return response;
    }

    private byte[] doRunLengthEncoding(byte[] source) {
        List<Byte> bytes = new ArrayList<>(source.length);

        byte prevSymbol = source[0];
        int chainLength = 1;
        List<Byte> differentSymbolsChain = new ArrayList<>();

        for (int i = 1; i < source.length; i++) {
            byte symbol = source[i];
            if (prevSymbol == symbol) {
                chainLength++;
                if (chainLength == 2 && differentSymbolsChain.size() != 0) {
                    int length = differentSymbolsChain.size();
                    addIntegerStringBytes(bytes, length == 1 ? 1 : -length);
                    bytes.addAll(differentSymbolsChain);
                    differentSymbolsChain.clear();
                }
            } else {
                if (chainLength != 1) {
                    addIntegerStringBytes(bytes, chainLength);
                    bytes.add(prevSymbol);
                    chainLength = 1;
                } else {
                    differentSymbolsChain.add(prevSymbol);
                }
                prevSymbol = symbol;
            }
        }
        differentSymbolsChain.add(prevSymbol);
        int length = differentSymbolsChain.size();
        if (length == 1) {
            addIntegerStringBytes(bytes, length);
            bytes.add(prevSymbol);
        } else {
            addIntegerStringBytes(bytes, -length);
            bytes.addAll(differentSymbolsChain);
        }

        return convertByteListToArray(bytes);
    }

    private byte[] convertByteListToArray(List<Byte> list) {
        byte[] array = new byte[list.size()];
        for (int i = 0; i < array.length; i++) {
            array[i] = list.get(i);
        }

        return array;
    }

    private void addIntegerStringBytes(List<Byte> target, int length) {
        byte[] lengthBytes = Integer.toString(length).getBytes();
        for (byte lengthByte : lengthBytes) {
            target.add(lengthByte);
        }
    }

    public byte[] doBwt(byte[] source) {
        int length = source.length;
        byte[][] matrix = new byte[length][length];
        matrix[0] = source;

        for (int i = 1; i < length; i++) {
            byte[] prev = matrix[i - 1];
            System.arraycopy(prev, 1, matrix[i], 0, length - 1);
            matrix[i][length - 1] = prev[0];
        }

        List<Byte> bytes = Arrays.stream(matrix)
                .sorted(new ByteArrayComparator())
                .map((element) -> element[length - 1]).collect(Collectors.toList());

        return convertByteListToArray(bytes);
    }

    public byte[] doHuffmanCoding(byte[] source) {
        Map<Byte, Integer> charsFrequencies = new HashMap<>();
        for (byte sourceByte : source) {
            int frequency = charsFrequencies.getOrDefault(sourceByte, 0);
            charsFrequencies.put(sourceByte, ++frequency);
        }

        List<Pair<Byte, Integer>> sortedCharsFrequencies = charsFrequencies
                .entrySet()
                .stream()
                .map((entry) -> new Pair<>(entry.getKey(), entry.getValue()))
                .sorted(Comparator.comparingInt(Pair::getSecond))
                .collect(Collectors.toList());

        PriorityQueue<HuffmanNode> nodes = new PriorityQueue<>(
                sortedCharsFrequencies.size(), Comparator.comparingInt(node -> node.frequency)
        );

        for (Pair<Byte, Integer> pair : sortedCharsFrequencies) {
            HuffmanNode node = new HuffmanNode();
            node.value = pair.getFirst();
            node.frequency = pair.getSecond();
            nodes.add(node);
        }

        HuffmanNode root = null;
        while (nodes.size() > 1) {
            HuffmanNode left = nodes.poll();
            HuffmanNode right = nodes.poll();

            assert right != null;
            root = new HuffmanNode();
            root.frequency = left.frequency + right.frequency;
            root.left = left;
            root.right = right;

            nodes.add(root);
        }

        StringBuilder builder = new StringBuilder();
        List<Pair<Byte, String>> codes = getHuffmanCodes(root);
        for (byte sourceByte : source) {
            Optional<Pair<Byte, String>> codePairOptional = codes
                    .stream()
                    .filter((codePair) -> codePair.getFirst() == sourceByte)
                    .findFirst();
            assert codePairOptional.isPresent();
            builder.append(codePairOptional.get().getSecond());
        }

        return parseBytes(builder.toString());
    }

    private List<Pair<Byte, String>> getHuffmanCodes(HuffmanNode root) {
        return getHuffmanCodes(root, "0");
    }

    private List<Pair<Byte, String>> getHuffmanCodes(HuffmanNode node, String code) {
        List<Pair<Byte, String>> codes = new ArrayList<>();
        if (node != null) {
            if (node.value == 0) {
                codes.addAll(getHuffmanCodes(node.left, code + "0"));
                codes.addAll(getHuffmanCodes(node.right, code + "1"));
            } else {
                codes.add(new Pair<>(node.value, code));
            }
        }

        return codes;
    }

    private byte[] parseBytes(String source) {
        byte[] bytes = new byte[source.length() / 8];
        for (int i = 0; i < bytes.length; i++) {
            int start = i * 8;
            String byteString = source.substring(start, start + 8);
            bytes[i] = Byte.parseByte(byteString.substring(1), 2);
            if (byteString.charAt(0) == '0') bytes[i] -= 128;
        }

        return bytes;
    }

    private static class ByteArrayComparator implements Comparator<byte[]> {
        @Override
        public int compare(byte[] first, byte[] second) {
            assert first.length == second.length;
            for (int i = 0; i < first.length; i++) {
                if (first[i] > second[i]) {
                    return  1;
                } else if (first[i] < second[i]) {
                    return  -1;
                }
            }

            return 0;
        }
    }

    private static class HuffmanNode {
        int frequency;
        byte value;
        HuffmanNode left, right;
    }
}
