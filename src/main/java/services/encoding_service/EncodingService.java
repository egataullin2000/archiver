package services.encoding_service;

import model.EncoderResponse;

public interface EncodingService {
    EncoderResponse encode(byte[] source);
}
